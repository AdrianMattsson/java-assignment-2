CREATE TABLE IF NOT EXISTS superhero (
    hero_id serial PRIMARY KEY,
    hero_name varchar(50) NOT NULL,
    hero_alias varchar(50) NOT NULL,
    hero_origin varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS assistant (
    assistant_id serial PRIMARY KEY,
    assistant_name varchar(50)
);

CREATE TABLE IF NOT EXISTS heropower (
    power_id serial PRIMARY KEY,
    power_name varchar(50),
    power_desc varchar(255)
);

INSERT INTO superhero (hero_name, hero_alias, hero_origin) VALUES ('Gregory', 'Good Gregster', 'Nebraska, USA');
INSERT INTO assistant (assistant_name) VALUES ('Karen');
INSERT INTO heropower (power_name, power_desc) VALUES ('Charm', 'Charms the criminals into submission.');



