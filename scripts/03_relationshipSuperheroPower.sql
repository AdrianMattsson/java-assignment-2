CREATE TABLE IF NOT EXISTS superhero_heropower (
    hero_id int,
    power_id int,
    PRIMARY KEY (hero_id, power_id)
);

ALTER TABLE superhero_heropower
ADD CONSTRAINT superhero_heropower_FK_hero FOREIGN KEY (hero_id) REFERENCES superhero,
ADD CONSTRAINT superhero_heropower_FK_power FOREIGN KEY (power_id) REFERENCES heropower;