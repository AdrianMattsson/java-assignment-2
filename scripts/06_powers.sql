INSERT INTO heropower (power_name, power_desc) VALUES ('Speed Robbing', 'Can rob 20 people under 5 minutes.');
INSERT INTO heropower (power_name, power_desc) VALUES ('Mega Moisting', 'Can fill any object with optional liquids.');
INSERT INTO heropower (power_name, power_desc) VALUES ('Super Complainer', 'Can win any argument by exhausting her opponents.');
INSERT INTO heropower (power_name, power_desc) VALUES ('Super Strength', 'Standard power of every superhero.');

INSERT INTO superhero_heropower (hero_id, power_id) VALUES (1,1);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (2,2);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (3,3);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (4,4);

INSERT INTO superhero_heropower (hero_id, power_id) VALUES (1,5);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (2,5);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (3,5);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (4,5);

INSERT INTO superhero_heropower (hero_id, power_id) VALUES (3,4);
INSERT INTO superhero_heropower (hero_id, power_id) VALUES (2,1);
