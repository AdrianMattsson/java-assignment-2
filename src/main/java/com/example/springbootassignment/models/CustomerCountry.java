package com.example.springbootassignment.models;

public record CustomerCountry(String name) {
    public String getName() {
        return name;
    }
}
