package com.example.springbootassignment.models;

public record CustomerSpender(int id, String name) {
}
