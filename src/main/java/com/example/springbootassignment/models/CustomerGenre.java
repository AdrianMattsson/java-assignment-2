package com.example.springbootassignment.models;

import java.util.ArrayList;

public record CustomerGenre(ArrayList<String> genreList) {

    public String getGenreInformation() {
        StringBuilder information = new StringBuilder();
        for (int i = 0; i < genreList.size(); i++) {
            information.append(genreList.get(i) +" ");
        }
        return information.toString();
    }
}
