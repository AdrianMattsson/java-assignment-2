package com.example.springbootassignment.models;

public record Customer(int id, String firstName, String lastName, String country, String postalCode, String phoneNumber, String email) {

    public String getCustomerInformation() {
        StringBuilder information = new StringBuilder();
        information.append(id + " ");
        information.append(firstName + " ");
        information.append(lastName + " ");
        information.append(country + " ");
        information.append(postalCode + " ");
        information.append(phoneNumber + " ");
        information.append(email + " ");
        return information.toString();
    }
}
