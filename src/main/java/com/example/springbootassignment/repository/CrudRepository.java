package com.example.springbootassignment.repository;

import com.example.springbootassignment.models.CustomerCountry;
import com.example.springbootassignment.models.CustomerGenre;
import com.example.springbootassignment.models.CustomerSpender;

import java.util.List;

public interface CrudRepository<T, ID> {

    /**
     * The findAll method finds all objects in a table in your chosen database.
     * @return List of all objects with their information.
     */
    List<T> findAll();

    /**
     * The findByName method finds objects which name matches the input parameter.
     * @param name is the name which it will match to objects.
     * @return list of objects which name matches the input parameter.
     */
    List<T> findByName(String name);

    /**
     * The getPageOfResults method gets a limited list of objects frm a table.
     * @param limit limits the result by given number (amount of objects in the list).
     * @param offset sets the starting point by a given number (i.e. starts at the 50th object).
     * @return list of objects limited and offset by a given number.
     */
    List<T> getPageOfResults(int limit, int offset);

    /**
     * The findById method finds an object which id matches the input parameter.
     * @param id is the id which you want to match with an object.
     * @return the object which matches the id.
     */
    T findById(ID id);

    /**
     * The insert method inserts a given object in a table.
     * @param object is the object that would be inserted.
     */
    void insert(T object);

    /**
     * The update method matches an object with the given id and then updates that object to match the properties of the given object.
     * @param id is the id for which the method will match to an already existing object.
     * @param object is the object which properties will replace current objects properties.
     */
    void update(int id, T object);
}
