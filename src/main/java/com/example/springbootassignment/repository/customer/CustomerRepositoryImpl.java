package com.example.springbootassignment.repository.customer;

import com.example.springbootassignment.models.Customer;
import com.example.springbootassignment.models.CustomerCountry;
import com.example.springbootassignment.models.CustomerGenre;
import com.example.springbootassignment.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{
    private String url;
    private String username;
    private String password;

    public CustomerRepositoryImpl(@Value("${spring.datasource.url}")String url,
                                  @Value("${spring.datasource.username}") String username,
                                  @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        Customer customer = null;
        String sql = "SELECT * FROM customer";
        List<Customer> customerList = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerList;
    }

    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    @Override
    public List<Customer> findByName(String customerName) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name = ?";
        List<Customer> customerList = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customerName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerList;
    }

    @Override
    public List<Customer> getPageOfResults(int limit, int offset) {
        Customer customer = null;
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        List<Customer> customerList = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1,limit);
            preparedStatement.setInt(2,offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerList;
    }

    @Override
    public void insert(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(int customerId, Customer updatedCustomer) {
        String sql = "UPDATE customer SET first_name=?, last_name=?, country=?, postal_code=?, phone=?, email=? WHERE customer_id = ?";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, updatedCustomer.firstName());
            preparedStatement.setString(2, updatedCustomer.lastName());
            preparedStatement.setString(3, updatedCustomer.country());
            preparedStatement.setString(4, updatedCustomer.postalCode());
            preparedStatement.setString(5, updatedCustomer.phoneNumber());
            preparedStatement.setString(6, updatedCustomer.email());
            preparedStatement.setInt(7, customerId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public CustomerCountry getCountryWithMostCustomers () {
        CustomerCountry country = null;
        String sql = "SELECT country FROM customer GROUP BY country ORDER BY COUNT(*) DESC LIMIT 1";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                country = new CustomerCountry(
                        resultSet.getString("country")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }

    @Override
    public CustomerSpender getHighestSpender(){
        CustomerSpender customerSpender = null;
        String sql = "SELECT customer.customer_id, customer.first_name FROM customer\n" +
                "JOIN invoice ON invoice.customer_id = customer.customer_id\n" +
                "GROUP BY customer.customer_id ORDER BY SUM(total) DESC LIMIT 1\n" + "\n";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerSpender = new CustomerSpender(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }
    
    @Override
    public CustomerGenre getCustomerGenre(int customerId) {
        CustomerGenre customerGenre = null;
        ArrayList<String> listOfGenres = new ArrayList<>();

        String sql = "SELECT genre.genre_id, genre.name FROM customer " +
                "INNER JOIN invoice ON customer.customer_id = invoice.customer_id " +
                "INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id " +
                "INNER JOIN track ON invoice_line.track_id = track.track_id " +
                "INNER JOIN genre ON track.genre_id = genre.genre_id " +
                "WHERE customer.customer_id = ? " +
                "GROUP BY genre.genre_id, genre.name " +
                "ORDER BY COUNT(genre.genre_id) DESC FETCH FIRST 1 ROWS WITH TIES ";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                listOfGenres.add(resultSet.getString("name"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        customerGenre = new CustomerGenre(listOfGenres);
        return customerGenre;
    }
}
