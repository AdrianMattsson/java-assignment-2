package com.example.springbootassignment.repository.customer;

import com.example.springbootassignment.models.Customer;
import com.example.springbootassignment.models.CustomerCountry;
import com.example.springbootassignment.models.CustomerGenre;
import com.example.springbootassignment.models.CustomerSpender;
import com.example.springbootassignment.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    /**
     * The getCountryWithMostCustomers method finds and returns the country in which most customers reside.
     * @return The country with the most customers.
     */
    CustomerCountry getCountryWithMostCustomers();

    /**
     * The getHighestSpender method finds and returns the customer whose total expenditure is the highest among customers.
     * @return The customer that has spent the most.
     */
    CustomerSpender getHighestSpender();

    /**
     * The getCustomerGenre method takes in an id for a customer and then returns the most bought genre of that customer.
     * @param customerId is the id which the method will match to a customer.
     * @return CustomerGenre object, which includes a list of the most bought genres of that customer.
     */
    CustomerGenre getCustomerGenre(int customerId);

}
