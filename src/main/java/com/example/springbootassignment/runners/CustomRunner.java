package com.example.springbootassignment.runners;

import com.example.springbootassignment.models.Customer;
import com.example.springbootassignment.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;
    public CustomRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(customerRepository.findByName("Patrick"));

        List<Customer> customerList = new ArrayList<>();
        customerList = customerRepository.getPageOfResults(10,5);
        customerList = customerRepository.findAll();
        customerList.forEach(customer -> System.out.println(customer.getCustomerInformation()));
        customerRepository.insert(new Customer(1, "Gregory", "Klostergrehn", "Netherlands", "82104", "0637881346", "greg.klost@gmail.com"));
        System.out.println(customerRepository.findByName("Gregory"));
        customerRepository.update(1, new Customer(0, "Sona", "Klostergrehn", "Netherlands", "82104", "0637881346", "greg.klost@gmail.com"));
        System.out.println(customerRepository.findById(1));
        System.out.println(customerRepository.getCountryWithMostCustomers().getName());
        System.out.println(customerRepository.getHighestSpender().name());
        System.out.println(customerRepository.getCustomerGenre(12).getGenreInformation());

        // customerRepository.findByName("Khent").forEach(x -> System.out.println(x.getCustomerInformation()));

    }
}
