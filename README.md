# Java Assignment 2 - Databases
This project was created as part of an assigment in Java. This application allows for the users to use multiple commands to get or modify information from/in a database.

## Installation

### Downloading
The application is free to clone straight from gitlab. Type this into your selected git console to get the current main version:
```
git clone git@gitlab.com:AdrianMattsson/java-assignment-2.git
```
Git will copy the repository onto your machine.

You will also need to download and run the Chinook database script. 
(Script is not included in the repository, a download will have to be found online.)



## Usage
### Running the application
The application is very simple to run, just compile it in your IDE of choice.

Edit the CustomRunner class file with your preferred alterations or methods and run the file to run the application. 

You can read about available methods and features in the javadoc for CrudRepository. The javadoc will also include instructions on how to properly use the methods. 



## Contributors

Adrian Mattsson @AdrianMattsson

Sona Rahimova @sona.rahimova
